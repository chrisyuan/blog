# 一年一度OceanBase技术征文大赛全面开启！ 入门实战，等您来写

OceanBase 作为完全自主研发的国产原生分布式数据库，已连续 9 年稳定支撑双 11，创新推出“三地五中心”城市级容灾新标准，是全球唯一在 TPC-C 和 TPC-H 测试上都刷新了世界纪录的国产原生分布式数据库。

为了让更多用户能够真正体验到 OceanBase 数据库，Gitee 联合 OceanBase 发起一年一度 OceanBase 技术征文大赛，邀请广大开发者体验从部署到迁移的各种玩法，第一期征文主题将围绕《OceanBase 社区版入门到实战》课程学习与实践。

时间周期大概为 2 个月，时间节点如下：

**文章投稿**：5月27日至7月25日

**入围初审**：6月1日至7月25日（每两周公布入围文章）

**专家评优**：7月26日至7月28日 

**评优公布**：7月29日

## 1、活动规则

**投稿内容：** 《OceanBase 社区版入门到实战》课程学习和实践分享，包括安装部署、开发运维、数据迁移、性能调优等使用技巧、学习笔记。

**课程学习地址：**

[https://open.oceanbase.com/docs/videoCenter](https://open.oceanbase.com/docs/videoCenter)

[https://open.oceanbase.com/docs/tutorials-cn/V1.0.0](https://open.oceanbase.com/docs/tutorials-cn/V1.0.0)

## 2、投稿方式

第1步：编写文章，提交pull request到OceanBase下的blog仓库，文章在Gitee社区首发，并添加标签 #一年一度OceanBase技术征文

第2步：命名格式：参赛方向（安装部署/开发运维/迁移实战/性能调优）+ 作品名称

第3步：图片存放：正文中用到的图片请统一放在 blog 的 media 目录下，正文中引用的图片命名避免用 1、2、3 之类的 避免冲突

第4步：提交后，[**登记信息**](https://survey.taobao.com/apps/zhiliao/TlfGohl5t)，方便活动后工作人员联系颁发奖品。

    
## 3、初审

作者在投稿后，评审组将进行初审，过审文章每周公布一次，并转载至 OceanBase 社区版【博客】“入门实战” 专栏上，作者获得 OceanBase 周边礼品。通过初审的文章可参与评优活动；未通过初审的文章，评审组将给出修改建议，修改后可再次提交报名。

## 4、评优

OceanBase 技术专家根据文章内容质量（包含但不限于主题新颖、结构完整、逻辑清晰、格式整洁、富有实际意义等维度），评选出一、二、三等奖及优秀奖。本次征文文章字数不少于 800 字，图文并茂、原创，且在 Gitee 首发；抄袭将视为无效。此外，我们还设置了最佳人气奖和最佳勤勉奖，作为特别奖励。



最佳人气奖：1名

最佳勤勉奖：3名，入围文章篇数最多 Top3 评优结果将通过 Gitee、OceanBase 微信公众号公布。

一等奖1名：苹果手表

二等奖3名：机械键盘

三等奖5名：蓝牙鼠标

优秀奖若干：《OceanBase 入门与实战》图书一本

最佳人气奖1名：蓝牙鼠标

最佳勤勉奖3名：机械键盘



获奖作品、著作权归作者所有， OceanBase 拥有使用权。
